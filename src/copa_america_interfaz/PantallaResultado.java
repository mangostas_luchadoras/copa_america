package copa_america_interfaz;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import copa_america_negocio.Equipo;
import copa_america_negocio.Grupo;

public class PantallaResultado extends JPanel 
{
	
	private static final long serialVersionUID = 1L;
	private JLabel imagenCentrinho;
	private JLabel imagenCopa1;
	private JLabel imagenCopa2;
	private JTextPane nombreGrupoA;
	private JTextPane nombreGrupoB;
	private JTextPane nombreGrupoC;
	private GraficoGrupo grupoA;
	private GraficoGrupo grupoB;
	private GraficoGrupo grupoC;
	private ArrayList<ImageIcon> imagenes;
	private JButton btnVolver;
	
	
	/**
	 * Crea una pantalla donde se muestra un Fixture con el 
	 * resultado correspondiente a los datos de la ventana.
	 * Con el Boton 'VOLVER' se regresa al Menu.
	 * @param VentanaCopaAmerica ventana donde se va a representar la Pantalla de Resultado
	 */
	public PantallaResultado(VentanaCopaAmerica ventana) 
	{		
		setLayout(null);
		setBackground(new Color(1,42,130));		
		setBounds(new Rectangle(0, 0, 900, 700));
		 
		imagenes = new ArrayList<ImageIcon>();
		grupoA = new GraficoGrupo();
		grupoB = new GraficoGrupo();
		grupoC = new GraficoGrupo();
		
		agregarImagenesCopas();
		agregarGIF();
		agregarNombresGrupos();
		agregarBotonVolver(ventana);
	}
	
	/**
	 * Agrega una imagen de la Copa America 2019 en cada esquina del 
	 * extremo superior de la pantalla.
	 */
	private void agregarImagenesCopas()
	{
		ImageIcon copa1 = new ImageIcon(this.getClass().getResource("/image/copa_america.png"));
		imagenCopa1 = new JLabel();
		imagenCopa1.setBounds(50, 25, 200, 204);
		imagenCopa1.setIcon(copa1);
		add(imagenCopa1);
		
		ImageIcon copa2 = new ImageIcon(this.getClass().getResource("/image/copa_america.png"));
		imagenCopa2 = new JLabel();
		imagenCopa2.setBounds(650, 25, 200, 204);
		imagenCopa2.setIcon(copa2);
		add(imagenCopa2);
	}	
	
	/**
	 * Agrega un GIF en el extremo superior de la pantalla.
	 */
	private void agregarGIF()
	{
		ImageIcon imagen = new ImageIcon(this.getClass().getResource("/image/centrinho.gif"));
		imagenCentrinho = new JLabel();
		imagenCentrinho.setBounds(290, 10, 320, 192);
		imagenCentrinho.setIcon(imagen);
		add(imagenCentrinho);
	}
	
	/**
	 * Agrega el Boton 'VOLVER' para regresar al Menu.
	 * @param VentanaCopaAmerica ventana
	 */
	private void agregarBotonVolver(VentanaCopaAmerica ventana)
	{
		btnVolver = new JButton("VOLVER");
		acomodarBoton(btnVolver);
		btnVolver.setBounds(350, 596, 200, 80);
		btnVolver.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				setVisible(false);
				ventana.mostrarMenu();
			}
		});
		add(btnVolver);
	}
	
	/**
	 * Configura las caracteristicas del Boton.
	 * @param JButton boton
	 */
	private void acomodarBoton(JButton boton)
	{
		boton.setBorder(null);
		boton.setBorderPainted(false);
		boton.setContentAreaFilled(false);
		boton.setAlignmentX(Component.CENTER_ALIGNMENT);
		boton.setForeground(Color.WHITE);
		boton.setHorizontalTextPosition(SwingConstants.CENTER);
		boton.setIcon(new ImageIcon(Menu.class.getResource("/image/boton_negro.png")));
		boton.setPressedIcon(new ImageIcon(Menu.class.getResource("/image/boton_verde.png")));
		boton.setFont(new Font("OCR A Extended", Font.BOLD, 30));
	}
	
	/**
	 * Agrega los nombres de los grupos A, B y C en la pantalla.
	 */
	private void agregarNombresGrupos()
	{
		nombreGrupoA = new JTextPane();
		nombreGrupoA.setBounds(70, 290, 175, 40);
		configurarTxtPane(nombreGrupoA, "GRUPO A");
		add(nombreGrupoA);
		
		nombreGrupoB = new JTextPane();
		nombreGrupoB.setBounds(370, 210, 175, 40);
		configurarTxtPane(nombreGrupoB, "GRUPO B");
		add(nombreGrupoB);
		
		nombreGrupoC = new JTextPane();
		nombreGrupoC.setBounds(670, 290, 175, 40);
		configurarTxtPane(nombreGrupoC, "GRUPO C");
		add(nombreGrupoC);
	}
	
	/**
	 * Configura el formato del panel de texto 'panelTexto'.
	 * @param JTextPane panelTexto, panel que sera configurado
	 * @param String texto, lo que se mostrara en el panel
	 */
	private void configurarTxtPane(JTextPane panelTexto, String texto) 
	{	
		panelTexto.setText(texto);
		panelTexto.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelTexto.setForeground(Color.WHITE);
		panelTexto.setBackground(new Color(1,42,130));	
		panelTexto.setFont(new Font("AdLib BT", Font.BOLD, 32));
		panelTexto.setEditable(false);
	}
	
	/**
	 * Hace visible los Equipos de la Pantalla de Resultado en una ventana.
	 * @param VentanaCopaAmerica ventana
	 */
	public void mostrarGrupos(VentanaCopaAmerica ventana)
	{
		for (Grupo g : ventana.getFixture())
			for(Equipo e: g.getEquipos())
				imagenes.add(e.getImagen());
		
		grupoA.setBounds(0, 340, 302, 320);
		grupoA.agregarImagenPais(0, imagenes.get(0));
		grupoA.agregarImagenPais(1, imagenes.get(1));
		grupoA.agregarImagenPais(2, imagenes.get(2));
		grupoA.agregarImagenPais(3, imagenes.get(3));
		add(grupoA);
		
		grupoB.setBounds(300, 260, 302, 320);
		grupoB.agregarImagenPais(0, imagenes.get(4));
		grupoB.agregarImagenPais(1, imagenes.get(5));
		grupoB.agregarImagenPais(2, imagenes.get(6));
		grupoB.agregarImagenPais(3, imagenes.get(7));
		add(grupoB);
		
		grupoC.setBounds(600, 340, 302, 320);
		grupoC.agregarImagenPais(0, imagenes.get(8));
		grupoC.agregarImagenPais(1, imagenes.get(9));
		grupoC.agregarImagenPais(2, imagenes.get(10));
		grupoC.agregarImagenPais(3, imagenes.get(11));
		add(grupoC);	
		
		imagenes = new ArrayList<ImageIcon>();
	}
}