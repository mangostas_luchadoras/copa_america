package copa_america_interfaz;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Fondo extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private JLabel fondo;
	
	/**
	 * Carga y dibuja una imagen de fondo.
	 * @param String rutaImagen
	 */
	public Fondo(String rutaImagen)
	{
		setBounds(0, 0, 900, 700);
		setLayout(null);
		fondo = new JLabel();
		fondo.setBounds(0, 0, 900, 700);
		ImageIcon imagen = new ImageIcon(this.getClass().getResource(rutaImagen));
		fondo.setIcon(imagen);
		add(fondo);
	}

	/**
	 * Quita la imagen del Fondo.
	 */
	protected void borrarFondo() 
	{
		remove(fondo);
	}
	
	/**
	 * Dibuja la imagen del Fondo.
	 */
	protected void pintarFondo() 
	{
		add(fondo);
	}
	
	/**
	 * Configura las medidas del margen de la imagen.
	 * @param entero x, coordenada x del margende la imagen
	 * @param entero y, coordenada y del margende la imagen
	 * @param entero ancho total del margen de la imagen
	 * @param entero alto total del margen de la imagen
	 */
	public void setBoundsFondo(int x, int y, int ancho, int alto)
	{
		fondo.setBounds(x, y, ancho, alto);
	}
}