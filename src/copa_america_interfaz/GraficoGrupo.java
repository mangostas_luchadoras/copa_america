package copa_america_interfaz;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridLayout;

public class GraficoGrupo extends JPanel 
{
	
	private static final long serialVersionUID = 1L;
	private JLabel [] paises;
	private static int tamanio = 4;

	
	/**
	 * Crea un panel con espacio para cuatro imagenes.
	 * Inicializa un Array para cuatroJlabels.
	 */
	public GraficoGrupo() {
		setLayout(new GridLayout(2, 2, 0, 0));
		setBackground(new Color(1,42,130));	
		paises = new JLabel [tamanio];
		for (int i=0; i<tamanio; i++)
			paises[i] = new JLabel();
	}
	
	/**
	 * Agrega una imagen en un Array de Jlabels dependiendo del indice.
	 * @param entero indice
	 * @param ImageIcon imagen
	 */
	public void agregarImagenPais(int indice, ImageIcon imagen)
	{
		paises[indice].setBounds(0, 0, 150, 155);
		paises[indice].setIcon(imagen);
		add(paises[indice]);
	}
}