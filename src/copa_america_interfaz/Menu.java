package copa_america_interfaz;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class Menu extends Fondo 
{
	private static final long serialVersionUID = 1L;
	private JButton btnRanking;
	private JButton btnCargar;
	
	/**
	 * Crea un menu de la aplicacion con dos botones.
	 * Boton 'RANKING FIFA' para ver el resultado con los puntos de la FIFA,
	 * Boton 'CARGAR PUNTOS' para ver el resultado con puntos asignados manualmente a cada Equipo.
	 * @param VentanaCopaAmerica ventana donde se va a representar el Menu
	 * @param String rutaImagen
	 */
	public Menu(VentanaCopaAmerica ventana, String rutaImagen) 
	{
		super(rutaImagen);
		super.borrarFondo();
		
		btnRanking = new JButton("RANKING FIFA");
		acomodarBoton(btnRanking);
		btnRanking.setBounds(50, 490, 200, 65);
		btnRanking.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				setVisible(false);
				ventana.cargarFixtureFifa();
				ventana.mostrarPantallaResultado();
				ventana.mostrarEquiposPantallaResultado();
			}
		});
		add(btnRanking);
		
		btnCargar = new JButton("CARGAR PUNTOS");
		acomodarBoton(btnCargar);
		btnCargar.setBounds(300, 490, 200, 65);
		btnCargar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				setVisible(false);
				ventana.mostrarPantallaPuntos();
				ventana.getCuadrosTexto().agregarPanelesTexto(ventana);
			}
		});
		add(btnCargar);
			
		super.pintarFondo();
	}
	
	/**
	 * Configura las caracteristicas del Boton.
	 * @param JButton boton
	 */
	private void acomodarBoton(JButton boton)
	{
		boton.setBorder(null);
		boton.setBorderPainted(false);
		boton.setContentAreaFilled(false);
		boton.setAlignmentX(Component.CENTER_ALIGNMENT);
		boton.setForeground(Color.WHITE);
		boton.setHorizontalTextPosition(SwingConstants.CENTER);
		boton.setIcon(new ImageIcon(Menu.class.getResource("/image/boton_azul.png")));
		boton.setPressedIcon(new ImageIcon(Menu.class.getResource("/image/boton_verde_chico.png")));
		boton.setFont(new Font("OCR A Extended", Font.BOLD, 22));
	}
}