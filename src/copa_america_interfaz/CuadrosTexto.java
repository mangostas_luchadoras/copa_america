package copa_america_interfaz;

import java.awt.Font;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class CuadrosTexto 
{
	private JTextField [] txt_grupos;
	
	/**
	 * Inicializa un Array de 12 JTextField para los puntos de los Equipos.
	 * @param VentanaCopaAmerica ventana
	 */
	CuadrosTexto(VentanaCopaAmerica ventana)
	{
		txt_grupos = new JTextField[12];
	}
	
	/**
	 * Agrega los Paneles de Texto en una ventana.
	 * @param VentanaCopaAmerica ventana
	 */
	public void agregarPanelesTexto(VentanaCopaAmerica ventana)
	{
		crearPanelesTexto();
		for (int i=0; i<txt_grupos.length;i++)
		{
			txt_grupos[i].setHorizontalAlignment(JTextField.CENTER);
			txt_grupos[i].setFont(new Font("FreeSans", Font.BOLD, 16));
			ventana.agregar(txt_grupos[i]);
		}
	}
	
	/**
	 * Crea los Paneles de Texto del 'Cuadros de Texto'
	 */
	private void crearPanelesTexto()
	{
		JTextField argentina = new JTextField();

		argentina.setBounds(35, 186, 105, 25);
		argentina.setName("Argentina");
		txt_grupos[0] =argentina;
		
		JTextField bolivia = new JTextField();
		bolivia.setBounds(62, 417, 105, 25);
		bolivia.setName("Bolivia");
		txt_grupos[1] = bolivia;

		JTextField brasil = new JTextField();
		brasil.setBounds(35, 656, 105, 25);
		brasil.setName("Brasil");
		txt_grupos[2] =brasil;
		
		JTextField chile = new JTextField();
		chile.setBounds(215, 186, 105, 25);		
		chile.setName("Chile");
		txt_grupos[3] = chile;
		
		JTextField colombia = new JTextField();
		colombia.setBounds(215, 656, 105, 25);
		colombia.setName("Colombia");
		txt_grupos[4] = colombia;
		
		JTextField ecuador = new JTextField();
		ecuador.setBounds(395, 186, 105, 25);
		ecuador.setName("Ecuador");
		txt_grupos[5] =ecuador;
		
		JTextField japon = new JTextField();
		japon.setBounds(395, 656, 105, 25);
		japon.setName("Japon");
		txt_grupos[6] =japon;
		
		JTextField paraguay = new JTextField();
		paraguay.setBounds(582, 186, 105, 25);
		paraguay.setName("Paraguay");
		txt_grupos[7] =paraguay;
		
		JTextField peru = new JTextField();
		peru.setBounds(582, 656, 105, 25);
		peru.setName("Peru");
		txt_grupos[8] =peru;
		
		JTextField quatar = new JTextField();
		quatar.setBounds(760, 186, 105, 25);
		quatar.setName("Quatar");
		txt_grupos[9] =quatar;
		
		JTextField uruguay = new JTextField();
		uruguay.setBounds(735, 417, 105, 25);
		uruguay.setName("Uruguay");
		txt_grupos[10] =uruguay;
		
		JTextField venezuela = new JTextField();
		venezuela.setBounds(760, 656, 105, 25);
		venezuela.setName("Venezuela");
		txt_grupos[11] =venezuela;
	}
	
	/**
	 * Remueve los Paneles de Texto en una ventana.
	 * @param VentanaCopaAmerica ventana
	 */
	public void borrarPanelesTexto(VentanaCopaAmerica ventana)
	{
		for (int i=0; i<txt_grupos.length;i++)
			ventana.remover(txt_grupos[i]);
	}
	
	/**
	 * Agrega los equipos con los puntos de los Cuadros de Texto de una ventana.
	 * @param VentanaCopaAmerica ventana
	 */
	public void agregarEquiposYcargarPuntos(VentanaCopaAmerica ventana)
	{ 		
		for (int i=0; i<txt_grupos.length;i++)
		{
			String str = txt_grupos[i].getText();
			int valor = Integer.MAX_VALUE;
			try
			{
				valor = Integer.parseInt(str);
			}
			catch (NumberFormatException e)
			{
				valor = Integer.parseInt(JOptionPane.showInputDialog("Ingresar un numero entero para: " + txt_grupos[i].getName()));
			}
			ventana.agregarEquipo(txt_grupos[i].getName(), valor, i);		
		}	
	}
	
	/**
	 * Hace visibles los Paneles de Texto de los 'Cuadros de Texto'.
	 */
	public void mostrarPanelesTexto()
	{
		for (int i=0; i<txt_grupos.length;i++)
			txt_grupos[i].setVisible(true);
	}	
}