package copa_america_interfaz;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import copa_america_negocio.Equipo;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class PantallaPuntos extends Fondo 
{
	private static final long serialVersionUID = 1L;
	private JButton btnContinuar;
	
	/**
	 * Crea una Pantalla de Puntos en una ventana.
	 * con un Boton 'CONTINUAR' para ver la Pantalla de Resultados,
	 * una vez cargados correctamente los puntos de cada Equipo.
	 * @param VentanaCopaAmerica ventana donde se va a representar la Pantalla de Puntos
	 * @param String rutaImagen
	 */
	public PantallaPuntos(VentanaCopaAmerica ventana, String rutaImagen) 
	{
		super(rutaImagen);
		super.borrarFondo();
		
		agregarBotonContinuar(ventana);

		super.pintarFondo();
	}
	
	/**
	 * Agrega el Boton 'CONTINUAR' para regresar al Menu.
	 * @param VentanaCopaAmerica ventana
	 */
	private void agregarBotonContinuar(VentanaCopaAmerica ventana)
	{
		btnContinuar = new JButton("CONTINUAR");
		acomodarBoton(btnContinuar);
		btnContinuar.setBounds(500, 290, 200, 80);
		btnContinuar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				ventana.getCuadrosTexto().agregarEquiposYcargarPuntos(ventana);
				if(valoresCorrectos(ventana.getEquipos())) 
				{
					setVisible(false);
					ventana.mejorFixture();
					ventana.getCuadrosTexto().borrarPanelesTexto(ventana);
					ventana.mostrarPantallaResultado();
					ventana.mostrarEquiposPantallaResultado();
				}
			}
		});
		add(btnContinuar);
	}
	
	/**
	 * Verifica que los valores asignados a los equipos sean correctos y devuelve un boolean.
	 * @param ArrayList equipos
	 * @return boolean true o false
	 */
	private boolean valoresCorrectos(ArrayList<Equipo> equipos)
	{
		for (Equipo e : equipos)
			if (e.getFuerza() == Integer.MAX_VALUE)
				return false;
		return true;
	}
	
	/**
	 * Configura las caracteristicas del Boton.
	 * @param JButton boton
	 */
	private void acomodarBoton(JButton boton)
	{
		boton.setBorder(null);
		boton.setBorderPainted(false);
		boton.setContentAreaFilled(false);
		boton.setAlignmentX(Component.CENTER_ALIGNMENT);
		boton.setForeground(Color.WHITE);
		boton.setHorizontalTextPosition(SwingConstants.CENTER);
		boton.setIcon(new ImageIcon(PantallaPuntos.class.getResource("/image/boton_negro.png")));
		boton.setPressedIcon(new ImageIcon(PantallaPuntos.class.getResource("/image/boton_verde.png")));
		boton.setFont(new Font("OCR A Extended", Font.BOLD, 30));
	}
}