package copa_america_interfaz;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import copa_america_negocio.Equipo;
import copa_america_negocio.Fixture;
import copa_america_negocio.GeneradorFixture;

public class VentanaCopaAmerica 
{
	private JFrame frame;
	private ArrayList <Equipo> equipos;
	private static int cantEquipos = 12;
	private GeneradorFixture generador;	
	private Fixture fixture;
	private Fondo presentacion;
	private Menu menu;
	private CuadrosTexto textos;
	private PantallaPuntos pantallaPuntos;
	private PantallaResultado pantallaResultado;
	
	/**
	 * Activa la aplicacion.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					VentanaCopaAmerica ventana = new VentanaCopaAmerica();
					ventana.frame.setVisible(true);
					
					Timer timer = new Timer(1000, new ActionListener()
					{
					    @Override  
						public void actionPerformed(ActionEvent evt)
					    {
					    	ventana.presentacion.setVisible(false);
					    	ventana.menu.setVisible(true);
					    }
					});
					timer.setRepeats(false);		
					timer.start();
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Crea la aplicacion.
	 */
	public VentanaCopaAmerica() 
	{
		initialize();
	}

	/**
	 * Inicializa el contenido del frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		
		equipos = new ArrayList<Equipo>();
		for (int i=0; i< cantEquipos;i++)
			equipos.add(null);
		generador = new GeneradorFixture();
		fixture = new Fixture();
	
		presentacion = new Fondo("/image/mangosta_presentacion.png");
		menu = new Menu(this,"/image/menu.png");
		textos = new CuadrosTexto(this);
		pantallaPuntos = new PantallaPuntos(this, "/image/copa_america_puntos.png");
		pantallaResultado = new PantallaResultado(this);
		
		configurarVentana();
		agregarComponentes();
		
		menu.setVisible(false);
		pantallaPuntos.setVisible(false);
		pantallaResultado.setVisible(false);
	}
	
	/**
	 * Agrega un equipo a la lista de equipos con la cantidad de 'puntos', 
	 * en la posicion  'indice' del Arreglo de equipos.
	 * @param String nombre
	 * @param entero puntos
	 * @param entero indice
	 */
	public void agregarEquipo(String nombre, int puntos, int indice)
	{
		equipos.set(indice,new Equipo(nombre, puntos));
	}
	
	/**
	 * Devuelve un fixture generado de la lista de equipos, 
	 * Teniendo encuenta que la minima disperción de cada grupo sea la mayor posible.
	 */
	public void mejorFixture()
	{	
		fixture= generador.generarMejorFixture(equipos);
	}
	
	/**
	 * Configura los valores y formato de la ventana.
	 */
	private void configurarVentana() 
	{
		frame.setBounds(200, 40, 900, 720);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaCopaAmerica.class.getResource("/image/mangosta_icon.jpeg")));
		frame.setTitle("Copa América 2019");
	}
	
	/**
	 * Agrega la Presentacion, el Menu, la Pantalla de Resultados y 
	 * la Pantalla de Puntos a la ventana.
	 */
	private void agregarComponentes()
	{
		frame.getContentPane().add(presentacion);
		frame.getContentPane().add(menu);
		frame.getContentPane().add(pantallaResultado);
		frame.getContentPane().add(pantallaPuntos);
	}
	
	/**
	 * Hace visible el Menu en la ventana.
	 */
	public void mostrarMenu()
	{
		menu.setVisible(true);
	}
	
	/**
	 * Hace visible la Pantalla de Puntos en la ventana.
	 */
	public void mostrarPantallaPuntos()
	{
		pantallaPuntos.setVisible(true);
	}
	
	/**
	 * Hace visible la Pantalla de Resultado en la ventana.
	 */
	public void mostrarPantallaResultado()
	{
		pantallaResultado.setVisible(true);
	}
	
	/**
	 * Hace visible los Equipos en la Pantalla de Resultado de la ventana.
	 */
	public void mostrarEquiposPantallaResultado()
	{
		pantallaResultado.mostrarGrupos(this);
	}
	
	/**
	 * Devuelve el ArrayList de Equipos de la ventana.
	 * @return ArrayList equipos
	 */
	public ArrayList<Equipo> getEquipos()
	{
		return equipos;
	}
	
	/**
	 * Devuelve el Fixture de la ventana.
	 * @return Fixture fixture
	 */
	public Fixture getFixture()
	{
		return fixture;
	}
	
	/**
	 * Devuelve los Cuadros de Texto de la ventana.
	 * @return CuadrosTexto textos
	 */
	public CuadrosTexto getCuadrosTexto()
	{
		return textos;
	}
	
	/**
	 * Agrega un 'componente' a la ventana.
	 * @param Component componente
	 */
	public void agregar(Component componente)
	{
		frame.getContentPane().add(componente);
	}
	
	/**
	 * Remueve un 'componente' de la ventana.
	 * @param Component componente
	 */
	public void remover(Component componente)
	{
		frame.getContentPane().remove(componente);
	}
	
	/**
	 * Carga en el Fixture de la ventana el Fixture con los puntos del Ranking de la FIFA
	 * guardados en el archivo "fifa_fixture.save".
	 */
	public void cargarFixtureFifa()
	{
		Fixture fix = null;
		try
		{
			FileInputStream fis = new FileInputStream("fifa_fixture.save");
			ObjectInputStream in = new ObjectInputStream(fis);
			fix = (Fixture) in.readObject();
			in.close();
		}
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(frame, ex.getMessage());
			fix = new Fixture();
		}	
		fixture = fix;
	}
}