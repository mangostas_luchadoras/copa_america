package copa_america_negocio;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.Test;

class GeneradorFixtureTest 
{

	@Test
	void testGenerarCombinaciones_1() 
	{
		Fixture.setCantidadGrupos(2);
		String[] paises = {"Argentina", "Bolivia", "Uruguay", "Chile", "Paraguay", "Colombia", "Peru", "Venezuela"};
		GeneradorFixture f = new GeneradorFixture();
		
		ArrayList<Fixture> fix = f.generarTodosLosFixtures(generarEquipos(paises));
		
		assertEquals(70, fix.size());
	}

	@Test
	void testGenerarCombinaciones_2() 
	{
		Fixture.setCantidadGrupos(3);
		String[] paises = {"Argentina", "Bolivia", "Uruguay", "Chile", "Paraguay", "Colombia", 
										"Peru", "Venezuela", "Ecuador", "Quatar", "Japon", "Brazil"};
		GeneradorFixture f = new GeneradorFixture();
		
		ArrayList<Fixture> fix = f.generarTodosLosFixtures(generarEquipos(paises));
		
		assertEquals(34650, fix.size());
	}
	
	@Test
	void testGenerarCombinaciones_3() 
	{
		Grupo.setCantidadEquipos(3);
		String[] paises = {"Argentina", "Bolivia", "Uruguay", 
							"Paraguay", "Colombia", "Peru",
							"Ecuador", "Quatar", "Japon"};
		GeneradorFixture f = new GeneradorFixture();
		
		ArrayList<Fixture> fix = f.generarTodosLosFixtures(generarEquipos(paises));
		
		assertEquals(1680, fix.size());
	}
	
	private Collection<Equipo> generarEquipos(String[] paises)
	{
		ArrayList<Equipo> e = new ArrayList<Equipo>();
		for(int i=0; i<paises.length; i++)
			e.add(new Equipo(paises[i], i+10));
		return e;
	}
}
