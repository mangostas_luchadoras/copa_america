package copa_america_negocio;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.junit.jupiter.api.Test;

class GeneradorGruposTest {

	@Test
	void testGenerarCombinaciones_1() 
	{
		Grupo.setCantidadEquipos(3);
		String[] paises = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		GeneradorGrupos g = new GeneradorGrupos();
		Set<Grupo> e = g.generarTodosGruposPosibles(generarEquipos(paises));
		
		assertEquals(4, e.size());
	}

	@Test
	void testGenerarCombinaciones_2() 
	{
		Grupo.setCantidadEquipos(3);
		String[] paises = {"Argentina", "Bolivia", "Uruguay", "Chile", "Paraguay", "Colombia", "Peru", "Venezuela"};
		GeneradorGrupos g = new GeneradorGrupos();
		Set<Grupo> e = g.generarTodosGruposPosibles(generarEquipos(paises));
		
		assertEquals(56, e.size());
	}
	
	@Test
	void testGenerarCombinaciones_3() 
	{
		Grupo.setCantidadEquipos(4);
		String[] paises = {"Argentina", "Bolivia", "Uruguay", "Chile", "Paraguay", "Colombia", "Peru", "Venezuela"};
		GeneradorGrupos g = new GeneradorGrupos();
		Set<Grupo> e = g.generarTodosGruposPosibles(generarEquipos(paises));
		
		assertEquals(70, e.size());
	}
	
	private Collection<Equipo> generarEquipos(String[] paises)
	{
		ArrayList<Equipo> e = new ArrayList<Equipo>();
		for(int i=0; i<paises.length; i++)
			e.add(new Equipo(paises[i], i+10));
		return e;
	}
}
