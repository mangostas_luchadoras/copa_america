package copa_america_negocio;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Grupo implements Serializable
{
	private Set<Equipo> grupo;
	private double dispersion;
	private static int tamanio = 4;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Crea una instancia de Grupo.
	 */
	public Grupo()
	{
		grupo = new HashSet<Equipo>();
		dispersion = 0.0;
	}
	
	/**
	 * Agrega un equipo al grupo si este no 
	 * se encuentra repetido.
	 * Devuelve true si se agrega y false 
	 * en caso contrario.
	 * @param e equipo a agregar.
	 * @return true si el grupo es agregado.
	 */
	public boolean agregarEquipo(Equipo e)
	{
		boolean ret = false;
		if(contiene(e))
			return false;
		if(grupo.size() < tamanio)
			ret = grupo.add(e);
		if(ret && grupo.size() == tamanio)
			dispersion = dispersion();
		return ret;
	}
	
	/**
	 * Devuelve true si el grupo contiene al 
	 * equipo que se pasa.
	 * @param e equipo a verificar si pertenece al grupo.
	 * @return true si el equipo esta contenido.
	 */
	private boolean contiene(Equipo e)
	{
		for(Equipo eq : grupo)
			if (e.equals(eq))
				return true;
		return false;
	}
	
	/**
	 * Calcula la dispercion del grupo.
	 * @return double representando la dispercion.
	 */
	private double dispersion() 
	{
		double suma = 0;
		double promedio = promedio();
		
		for (Equipo equipo : grupo) 
			suma += Math.pow(equipo.getFuerza() - promedio, 2);
		suma = suma / (grupo.size() - 1);
		
		return suma;
	}

	/**
	 * Calcula el promedio de las fuerzas de los
	 *  equipo del grupo
	 * @return double promedio de las fuerzas.
	 */
	private double promedio() 
	{
		double promedio = 0;
		
		for (Equipo equipo : grupo) 
			promedio += equipo.getFuerza();
		promedio = promedio / grupo.size();
		
		return promedio;
	}
	
	/**
	 * Dvuelve true si el grupo g puede ser 
	 * emparejado con this grupo.
	 * @param g grupo a emparejar.
	 * @return true si son compatibles.
	 */
	public boolean grupoCompatible(Grupo g)
	{
		for(Equipo eq : grupo)
			if (g.contiene(eq))
				return false;
		return true;
	}
	
	/**
	 * Devuelve la cantidad de equipos que 
	 * conforman un grupo.
	 * @return int cantidad de equipos por grupo.
	 */
	public static int cantidadEquipos()
	{
		return tamanio;
	}
	
	/**
	 * Setea la cantidad de equipos necesarios 
	 * para formar un grupo.
	 * @param cantidadEquipos de los grupos.
	 */
	public static void setCantidadEquipos(int cantidadEquipos)
	{
		tamanio = cantidadEquipos;
	}
	
	/**
	 * Devuelve la cantidad actual de equipos que 
	 * contiene el grupo.
	 * @return tamanio del grupo.
	 */
	public int getTamanio()
	{
		return grupo.size();
	}
	
	/**
	 * Devuelve un Set con los equipos que 
	 * conforman el grupo.
	 * @return Set Equipos.
	 */
	public Set<Equipo> getEquipos()
	{
		return grupo;
	}
	
	/**
	 * Develve la dispercion del grupo.
	 * @return double representando la 
	 * dispercion del grupo.
	 */
	public double getDispersion()
	{
		return dispersion;
	}
	
	/**
	 * Devuelve la representacion como String 
	 * del grupo.
	 * @return grupo como String
	 */
	@Override
	public String toString()
	{
		StringBuilder ret = new StringBuilder();
		ret.append("╠══════════╣");
		ret.append("\n");
		for (Equipo equipo : grupo) 
		{
			ret.append("║");
			ret.append(String.format("%1$-9s %2$s",equipo.toString() , "║"));
			ret.append("\n");
		}
		ret.append("╠══════════╣");
		ret.append("\n");
		return ret.toString();
	}

	/**
	 * Comprar dos grupos en base a sus equipos, 
	 * devolviendo true si 
	 * todos ellos son iguales.
	 * @param obj el objeto con el que se quiere 
	 * comparar. 
	 * @return true si ambos grupos tienen los 
	 * mismo equipos.
	 */
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Grupo)) 
			return false;
		
		Grupo other = (Grupo) obj;
		
		boolean ret = true;
		
		for (Equipo equipo : grupo) 
			ret = ret && other.contiene(equipo);
		
		return ret;
	}
}
