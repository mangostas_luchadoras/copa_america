package copa_america_negocio;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class EquipoTest {

	@Test
	void testEqualsMismo() 
	{
		Equipo e = new Equipo("Argentina", 10);
		
		assertTrue(e.equals(e));
	}
	
	@Test
	void testEqualsDistintos() 
	{
		Equipo e = new Equipo("Argentina", 10);
		Equipo f = new Equipo("Bolivia", 10);
		
		assertFalse(e.equals(f));
		assertFalse(f.equals(e));
	}

	@Test
	void testEqualsIguales() 
	{
		Equipo e = new Equipo("Argentina", 10);
		Equipo f = new Equipo("Argentina", 12);
		
		assertTrue(e.equals(f));
		assertTrue(f.equals(e));
	}
}
