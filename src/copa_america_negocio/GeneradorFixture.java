package copa_america_negocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class GeneradorFixture implements Serializable
{
	private ArrayList<Fixture> coleccionesPosibles;
	private Fixture coleccionActual;
	private int tamanio;
	private GeneradorGrupos generadorGrupos;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Crea una instancia de GeneradorFixture
	 */
	public GeneradorFixture()
	{
		coleccionesPosibles = new ArrayList<Fixture>();
		coleccionActual = new Fixture();
		tamanio = Fixture.cantidadGrupos();
		generadorGrupos = new GeneradorGrupos();
	}
	
	/**
	 * Devuelve un ArrayList conteniendo todos los 
	 * fixtures posibles que se pueden generar a 
	 * partir de la coleccion de Equipos.
	 * @param coleccion de equipos que conformaran 
	 * los fixtures.
	 * @return ArrayList con todos los fixtures 
	 * posibles.
	 */
	public ArrayList<Fixture> generarTodosLosFixtures(Collection<Equipo> coleccion)
	{
		coleccionesPosibles = new ArrayList<Fixture>();
		generarFixtures(coleccion);
		return coleccionesPosibles;
	}
	
	/**
	 * Devuelve el mejor fixture en base a las 
	 * valoraciones que se puede formar con los 
	 * equipo de la coleccion. 
	 * @param coleccion de equipos que conformaran 
	 * el fixture.
	 * @return fixture con mejor valoracion.
	 */
	public Fixture generarMejorFixture(Collection<Equipo> coleccion) 
	{
		coleccionesPosibles = new ArrayList<Fixture>();
		generarFixtures(coleccion);
		return conseguirMejor();
	}

	/**
	 * Genera todos los fixtures posibles y los guarda en coleccionesPosibles.
	 * @param coleccion lista de equipo con los que generar los fixtures.
	 */
	private void generarFixtures(Collection<Equipo> coleccion) {
		Set<Grupo> grupos = generadorGrupos.generarTodosGruposPosibles(coleccion);
		ArrayList<Grupo> lista = grupos.stream()
								.collect(Collectors 
				                        .toCollection(ArrayList::new));
		
		ArrayList<ArrayList<Grupo>> validos = new ArrayList<ArrayList<Grupo>>();
		for (int i = 0; i < lista.size(); i++) 
		{
			Grupo g = lista.get(i);
			ArrayList<Grupo> aux = generarValidos(lista, g);
			
			validos.add(i, aux);
		}
		
		for (int i = 0; i < lista.size(); i++) 
		{
			coleccionActual.agregarGrupo(lista.get(i));
			ArrayList<Grupo> validos_i = validos.get(i);
			subgrupos(coleccionActual, validos_i, lista.get(i), tamanio);
		}
	}

	/**
	 * Devuelve un ArrayLis de grupos conteniendo 
	 * todos los grupos de la </i>lista</i>.
	 * @param lista de grupos a emparejar.
	 * @param g grupo con el que se emparejara.
	 * @return ArrayList con todos los grupos que 
	 * se pueden emparejar con el grupo </i>g</i>.
	 */
	private ArrayList<Grupo> generarValidos(ArrayList<Grupo> lista, Grupo g)
	{
		ArrayList<Grupo> aux = new ArrayList<Grupo>();
		aux = lista.stream()
			 .filter(p -> p.grupoCompatible(g))
			 .collect(Collectors 
                     .toCollection(ArrayList::new));
		return aux;
	}

	/**
	 * Genera todos los fixtures posibles para el
	 *  </i>grupo</i> dado. 
	 * @param fix Fixture que se esta generando.
	 * @param validos lista de grupos validos 
	 * para </i>grupo</i>.
	 * @param grupo en base al cual se generaran 
	 * los fixtures.
	 * @param tamanio la cantidad de grupos que 
	 * tienen los fixtures.
	 */
	private void subgrupos(Fixture fix, ArrayList<Grupo> validos, Grupo grupo, int tamanio)
	{
		for (int i = 0; i<validos.size();i++) 
		{
			Grupo g = validos.get(i);
			fix.agregarGrupo(g);		
			if (fix.getTamanio() == tamanio)
			{
				coleccionesPosibles.add(fix);
				fix = new Fixture();
				fix.agregarGrupo(grupo);
			}
			else
			{	
				ArrayList<Grupo> aux = generarValidos(validos, g);
				subgrupos(fix, aux, grupo, tamanio);
			}
		}
	}
	
	/**
	 * Devuelve el primer Fixture cuya valoracion sea maxima.
	 * @return Fixture con valoracion maxima.
	 */
	private Fixture conseguirMejor()
	{
		Fixture ret = new Fixture();
		
		double max = coleccionesPosibles.parallelStream()
				.mapToDouble(Fixture::getValoracion)
				.max().getAsDouble();
			
		for (Fixture f : coleccionesPosibles)
			if (f.getValoracion() == max) 
			{
				ret = f;
				break;
			}
		return ret;
	}
	
}