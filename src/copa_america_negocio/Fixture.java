package copa_america_negocio;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.OptionalDouble;
import java.util.Set;

public class Fixture implements Serializable, Iterable<Grupo>
{
	private Set<Grupo> fixture; 
	private double valoracion;
	private static int tamanio = 3;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Crea una instancia de fixture.
	 */
	public Fixture()
	{
		fixture = new HashSet<Grupo>();
		valoracion = 0;
	}

	/**
	 * Agrega un grupo al fixture si este no
	 *  se encuentra repetido.
	 * Devuelve true si se agrega y false en
	 *  caso contrario.
	 * @param grupo a agregar.
	 * @return true si el grupo es agregado.
	 */
	public boolean agregarGrupo(Grupo grupo) 
	{
		boolean ret = false;
		
		if (contiene(grupo))
			return false;
		if(getTamanio() < cantidadGrupos())
			ret = fixture.add(grupo);
		if(ret && getTamanio() == cantidadGrupos())
			valoracion = calcularValoracion();
		
		return ret;
	}
	
	/**
	 * Toma como valoracion la menor de las 
	 * disperciones de 
	 * los equipos que lo componen.
	 * @return un double representando la 
	 * menor de las disperciones 
	 * de los equipos que lo componen.
	 */
	private double calcularValoracion() 
	{
		OptionalDouble ret = fixture.stream()
							.mapToDouble(Grupo::getDispersion)
							.min();
		
		return ret.getAsDouble();
	}
	
	/**
	 * Develve la valoracion del fixture.
	 * @return double representando la valoracion.
	 */
	public double getValoracion()
	{
		return valoracion;
	}
	
	/**
	 * Devuelve la cantidad de grupos que 
	 * conforman los fixtures.
	 * @return el tamanio de los fixtures.
	 */
	public static int cantidadGrupos()
	{
		return tamanio;
	}
	
	/**
	 * Setea la cantidad de grupos necesarios 
	 * para formar un fixture
	 * @param cantidadGrupos de los fixtures
	 */
	public static void setCantidadGrupos(int cantidadGrupos)
	{
		tamanio = cantidadGrupos;
	}
	
	/**
	 * Devuelve true si el fixture contiene al 
	 * grupo q se pasa.
	 * @param g grupo a verificar si pertenece 
	 * al fixture.
	 * @return true si el grupo esta contenido.
	 */
	public boolean contiene(Grupo g)
	{
		for (Grupo grupo : fixture) 
			if(grupo.equals(g))
				return true;
		
		return false;
	}
	
	/**
	 * Devuelve la cantidad actual de grupos que
	 *  contiene el fixture.
	 * @return tamanio del fixture.
	 */
	public int getTamanio()
	{
		return fixture.size();
	}
	
	/**
	 * Devuelve la representacion como String 
	 * del fixture.
	 * @return fixture como String
	 */
	@Override
	public String toString()
	{
		StringBuilder ret = new StringBuilder();
		ret.append("\n");
		ret.append("╔══════════╗");
		ret.append("\n");
		
		for (Grupo grupo : fixture) 
			ret.append(grupo.toString());

		ret.append("╚══════════╝");
		ret.append("\n");
		return ret.toString();
	}
	/**
	 * Comprar dos fixtures en base a sus grupos, 
	 * devolviendo true si 
	 * todos ellos son iguales.
	 * @param obj el objeto con el que se quiere 
	 * comparar. 
	 * @return true si ambos fixtures tienen los 
	 * mismo grupos.
	 */
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Fixture)) 
			return false;
		
		Fixture other = (Fixture) obj;
		boolean ret = true;
		for (Grupo grupo : fixture) {
			boolean ret_aux = false;
			ret_aux = ret_aux || other.contiene(grupo);
			ret = ret && ret_aux;
		}
		
		return ret;
	}

	/**
	 * Devuelve un Iterator de los grupos del 
	 * fixture.
	 * @return iterator del fixture.
	 */
	@Override
	public Iterator<Grupo> iterator() {
		return fixture.iterator();
	}
	
}
