package copa_america_negocio;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class GeneradorGrupos implements Serializable
{
	private Set<Grupo> coleccionesPosibles;
	private Grupo coleccionActual;
	private int tamanio;
	private static final long serialVersionUID = 1L;

	/**
	 * Crea una instancia de GeneradorGrupos.
	 */
	public GeneradorGrupos()
	{
		coleccionesPosibles = new HashSet<Grupo>();
		coleccionActual = new Grupo();
		tamanio = Grupo.cantidadEquipos();
	}

	/**
	 * Genera todos los grupos posibles que se 
	 * pueden formar con los equipos de la 
	 * Collection que resive.
	 * @param coleccion de equipos a agrupar.
	 * @return Set con todos los grupos validos 
	 * que se pueden generar
	 */
	public Set<Grupo> generarTodosGruposPosibles(Collection<Equipo> coleccion) 
	{
		boolean [] usados= new boolean [coleccion.size()];
		Equipo[] lista = new Equipo[coleccion.size()];
		
		Iterator<Equipo> it = coleccion.iterator();
		for (int i =0; i<coleccion.size(); i++) 
		{
			lista[i] = it.next();
			usados[i] = false;
		}
		
		subGrupos(lista, tamanio, 0, 0, usados);
		
		return coleccionesPosibles;
	}

	/**
	 * Genera recursivamente los grupos a partir 
	 * de un array de equipos.
	 * @param lista array de equipos.
	 * @param tamanio de equipos por grupo.
	 * @param principio indice desde donde tomar
	 *  equipos
	 * @param tamanioActual cantidad de equipos 
	 * usados en ese momento.
	 * @param usados array de booleans que 
	 * representa si un equipo fue usado o no.
	 */
	private void subGrupos(Equipo[] lista, int tamanio, int principio, int tamanioActual, boolean[] usados) 
	{
		if (tamanioActual == tamanio) 
		{
			for (int i = 0; i < lista.length; i++) 
				if (usados[i] == true)
					coleccionActual.agregarEquipo(lista[i]);
			if(coleccionesPosibles.contains(coleccionActual) == false) 
			{
				coleccionesPosibles.add(coleccionActual);
				coleccionActual = new Grupo();	
			}
			return;
		}
		
		if (principio == lista.length) 
			return;
					
		usados[principio] = true;
		subGrupos(lista, tamanio, principio + 1, tamanioActual + 1, usados);

		usados[principio] = false;
		subGrupos(lista, tamanio, principio + 1, tamanioActual, usados);
	}
}
