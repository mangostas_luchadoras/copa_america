package copa_america_negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FixtureTest {

	@Test
	void testAgregarGrupo() 
	{
		String[] paises = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		Grupo g = generarGrupo(paises);
		Fixture f = new Fixture();
		
		assertTrue(f.agregarGrupo(g));
	}
	
	@Test
	void testAgregarGrupoRepetido() 
	{
		String[] paises = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		Grupo g = generarGrupo(paises);
		Fixture f = new Fixture();
		
		assertTrue(f.agregarGrupo(g));
		assertFalse(f.agregarGrupo(g));
	}

	@Test
	void testAgregarGrupoFixtureLleno() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		String[] paises2 = {"Paraguay", "Colombia", "Ecuador", "Venezuela"};
		String[] paises3 = {"Brazil", "Japon", "Quatar", "Peru"};
		String[] paises4 = {"Alemania", "España", "Francia", "Escosia"};
		
		Fixture f = generarFixture(paises1, paises2, paises3);
		Grupo g = generarGrupo(paises4); 
		
		assertFalse(f.agregarGrupo(g));
	}

	@Test
	void testContieneTrue() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		String[] paises2 = {"Paraguay", "Colombia", "Ecuador", "Venezuela"};
		String[] paises3 = {"Brazil", "Japon", "Quatar", "Peru"};
		
		Fixture f = generarFixture(paises1, paises2, paises3);
		Grupo g = generarGrupo(paises1);
		
		assertTrue(f.contiene(g));
	}
		
	@Test
	void testContieneFalse() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		String[] paises2 = {"Paraguay", "Colombia", "Ecuador", "Venezuela"};
		String[] paises3 = {"Brazil", "Japon", "Quatar", "Peru"};
		String[] paises4 = {"Alemania", "España", "Francia", "Escosia"};
		
		Fixture f = generarFixture(paises1, paises2, paises3);
		Grupo h = generarGrupo(paises4);
		
		assertFalse(f.contiene(h));
	}
	
	@Test
	void testEqualsMismoFixture() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		String[] paises2 = {"Paraguay", "Colombia", "Ecuador", "Venezuela"};
		String[] paises3 = {"Brazil", "Japon", "Quatar", "Peru"};
		
		Fixture f = generarFixture(paises1, paises2, paises3);
		
		assertTrue(f.equals(f));
	}

	@Test
	void testEqualsFixtureIguales() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		String[] paises2 = {"Paraguay", "Colombia", "Ecuador", "Venezuela"};
		String[] paises3 = {"Brazil", "Japon", "Quatar", "Peru"};
		
		Fixture f = generarFixture(paises1, paises2, paises3);
		Fixture g = generarFixture(paises2, paises1, paises3);
		
		assertTrue(f.equals(g));
		assertTrue(g.equals(f));
	}
	
	@Test
	void testEqualsFixtureDistintos() 
	{
		String[] paises1 = {"Japon", "Uruguay", "Colombia", "Chile"};
		String[] paises2 = {"Bolivia", "Ecuador", "Brasil", "Venezuela"};
		String[] paises3 = {"Quatar", "Paraguay", "Peru", "Arentina"};
		
		String[] paises4 = {"Paraguay", "Bolivia", "Colombia", "Arentina"};
		String[] paises5 = {"Japon", "Quatar", "Peru", "Brasil"};
		String[] paises6 = {"Uruguay", "Ecuador", "Venezuela", "Chile"};
		
		Fixture f = generarFixture(paises1, paises2, paises3);
		Fixture g = generarFixture(paises4, paises5, paises6);
		
		assertFalse(f.equals(g));
		assertFalse(g.equals(f));
	}
	
	private Grupo generarGrupo(String[] paises)
	{
		Grupo g = new Grupo();
		
		for(int i=0; i<paises.length; i++)
			g.agregarEquipo(new Equipo(paises[i], i+10));
		
		return g;
	}
	
	private Fixture generarFixture(String[] paises1, String[] paises2, String[] paises3) 
	{
		Fixture f = new Fixture();
		Grupo g1 = generarGrupo(paises1);
		Grupo g2 = generarGrupo(paises2);
		Grupo g3 = generarGrupo(paises3);
		
		f.agregarGrupo(g1);
		f.agregarGrupo(g2);
		f.agregarGrupo(g3);
		
		return f;
	}
}
