package copa_america_negocio;

import java.io.Serializable;
import javax.swing.ImageIcon;

public class Equipo implements Serializable
{
	private String nombre;
	private int fuerza;
	private ImageIcon imagen;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Crea una instancia de Equipo
	 * @param equipo String con el nombre del equipo.
	 * @param puntos int representando la fuerza del equipo.
	 */
	public Equipo(String equipo, int puntos)
	{
		nombre = equipo;
		fuerza = puntos;
		try
		{
			imagen = new ImageIcon(this.getClass().getResource("/image/"+ equipo +".png"));
		}
		catch (NullPointerException e)
		{
			imagen = new ImageIcon(this.getClass().getResource("/image/Republica_Mangosta.png"));
		}		
	}

	/**
	 * Devuelve el nombre del equipo.
	 * @return el nombre del equipo
	 */
	public String getNombre() 
	{
		return nombre;
	}

	/**
	 * Devuelve la fuerza del equipo.
	 * @return int representando la fuerza del equipo.
	 */
	public int getFuerza() 
	{
		return fuerza;
	}
	
	/**
	 * Devuelve el IconImage con la bandera del equipo.
	 * @return IconImage de la bandera.
	 */
	public ImageIcon getImagen()
	{
		return imagen;
	}
	
	/**
	 * Devuelve el String con el nombre del equipo.
	 * @return nombre del equipo.
	 */
	@Override
	public String toString()
	{
		return nombre;
	}
	
	/**
	 * Comprar dos equipo en base a sus nombres.
	 * @param obj el objeto con el que se quiere comparar. 
	 * @return true si ambos equipos tienen el mismo nombre.
	 */
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Equipo)) 
			return false;
		
		Equipo other = (Equipo) obj;
		
		return (nombre.equals(other.getNombre())) ;
	}
}
