package copa_america_negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GrupoTest 
{

	@Test
	void testAgregarEquipo() 
	{
		Grupo g = new Grupo();
		Equipo e = new Equipo("Argentina", 10);
		assertTrue(g.agregarEquipo(e));		
	}
	
	@Test
	void testAgregarEquipoGrupoLleno() 
	{
		String[] paises = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		Grupo g = generarGrupo(paises);
		Equipo j = new Equipo("Paraguay", 14);
		
		assertFalse(g.agregarEquipo(j));		
	}
	
	@Test
	void testAgregarEquipoRepetido() 
	{
		String[] paises = {"Argentina", "Bolivia", "Uruguay"};
		Grupo g = generarGrupo(paises);
		Equipo e = new Equipo("Argentina", 10);
		
		assertFalse(g.agregarEquipo(e));		
	}

	@Test
	void testEqualsMismoGrupo() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		Grupo g = generarGrupo(paises1);
		
		assertTrue(g.equals(g));
	}
	
	@Test
	void testEqualsGruposDistintos() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		Grupo g = generarGrupo(paises1);
		String[] paises2 = {"Paraguay", "Colombia", "Ecuador", "Venezuela"};
		Grupo h = generarGrupo(paises2);
		
		assertFalse(g.equals(h));
		assertFalse(h.equals(g));
	}
	
	@Test
	void testEqualsGruposIguales() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		Grupo g = generarGrupo(paises1);
		String[] paises2 = {"Chile", "Uruguay", "Bolivia", "Argentina"};
		Grupo h = generarGrupo(paises2);
		
		assertTrue(g.equals(h));
		assertTrue(h.equals(g));
	}
	
	@Test
	void testGrupoCompatibleFalse() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		Grupo g = generarGrupo(paises1);
		String[] paises2 = {"Paraguay", "Colombia", "Bolivia", "Venezuela"};
		Grupo h = generarGrupo(paises2);
		
		assertFalse(g.grupoCompatible(h));
		assertFalse(h.grupoCompatible(g));
	}

	@Test
	void testGrupoCompatibleTrue() 
	{
		String[] paises1 = {"Argentina", "Bolivia", "Uruguay", "Chile"};
		Grupo g = generarGrupo(paises1);
		String[] paises2 = {"Paraguay", "Colombia", "Peru", "Venezuela"};
		Grupo h = generarGrupo(paises2);
		
		assertTrue(g.grupoCompatible(h));
		assertTrue(h.grupoCompatible(g));
	}
	
	private Grupo generarGrupo(String[] paises)
	{
		Grupo g = new Grupo();
		for(int i=0; i<paises.length; i++)
			g.agregarEquipo(new Equipo(paises[i], i+10));
		return g;
	}
}
